package utilities;

import org.openqa.selenium.*;

import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Actions;

import java.util.Set;

public class SeleniumHelpers {
    int webDriverDuration = 60;
    int PAGE_LOAD_WAIT_DURATION = 60;
    WebDriver driver;
    Actions actions;

    public SeleniumHelpers(WebDriver driver) {
        this.driver = driver;
        actions = new Actions(driver);
    }

    /**
     * Open url
     * @url Website main page
     */
    public void navigateToPage(String url) {
        driver.get(url);
    }

    /**
     * o wait until element is visible
     * @param e WebElement object
     * @return WebElement object
     */
    public WebElement waitTillElementIsVisible(WebElement e)
    {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(e));
        return e;
    }

    /**
     * To wait until element is clickable
     * @param e WebElement object
     * @return WebElement object
     */
    public WebElement waitTillElementIsClickable(WebElement e) {
        WebDriverWait wait = new WebDriverWait(driver, webDriverDuration);
        wait.until(ExpectedConditions.elementToBeClickable(e));
        return e;
    }

    /**
     * To wait until element is clickable
     * @param by By object
     * @return WebElement object
     */
    public WebElement waitTillElementIsClickable(By by) {
        WebDriverWait wait = new WebDriverWait(driver, webDriverDuration);
        return wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    /**
     * This function will wait for page to load (waiting for java script to finish loading) before moving further
     * @throws InterruptedException
     * @paramWaitTime Maximum time is the time out time. if the page loading completes before timeout, code will process
     */
    public void waitForJavascriptToLoad() throws InterruptedException {
        Thread.sleep(1000);
        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
            }
        };
        Wait<WebDriver> wait = new WebDriverWait(driver, webDriverDuration);
        try {
            wait.until(expectation);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Error e) {
            e.printStackTrace();
        }
    }

    /**
     * Enter text to input field
     * @param by    By object
     * @param text  input text
     * @param clear set true if want to clear field else set false
     */
    public void enterText(By by, String text, boolean clear) {
        WebElement e = waitTillElementIsClickable(by);
        if (clear) {
            e.clear();
        }
        e.sendKeys(text);
    }

    /**
     * Click on Element
     * @param by By object
     * @throws InterruptedException
     */
    public void clickOn(By by) throws InterruptedException {
        waitTillElementIsClickable(by).click();
        waitForJavascriptToLoad();
    }

    /**
     * Get Current URL
     */
    public String getURL() {
        return driver.getCurrentUrl();
    }

    /**
     * WindowHandles
     */
    public Set<String> getWindowHandles() {
        return driver.getWindowHandles();
    }

    /**
     * Switch window
     * @param tabNumber is a number of tab
     */
    public void switchToWindow(int tabNumber) {
        int i = 1;
        for (String winHandle : getWindowHandles())
        {
            driver.switchTo().window(winHandle);
            if (i == tabNumber)
                break;
            i++;
        }
    }
}
