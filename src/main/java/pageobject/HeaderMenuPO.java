package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

public class HeaderMenuPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public HeaderMenuPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//div[@class='menuchild-parent desktop']/a[text()='Investing']")
    private WebElement investingMenu;

    /**
     * Click on header menu
     * @menu emailOrPassword
     */
    public void clickOnMenu(String menu) throws InterruptedException {
        selenium.clickOn(By.xpath("//div[@class='menuchild-parent desktop']//a[text()='" + menu + "']"));
    }
}
