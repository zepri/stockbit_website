package runners;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/stockbit/cucumber-report.json",  "html:target/results/stockbit"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@header-menu"}

)
public class StockbitTestRunner extends BaseTestRunner
{

}
