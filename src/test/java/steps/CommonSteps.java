package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class CommonSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    String basicUrl = "https://stockbit.com";

    @Given("user access main page stockbit")
    public void user_access_main_page_stockbit() {
        selenium.navigateToPage(basicUrl);
    }

    @Then("system direct to {string} page")
    public void system_direct_to_page(String page) throws InterruptedException {
        switch (page.toLowerCase()) {
            case "start investing":
                page = "/info/mulai-investasi";
                break;
            case "professional tools":
                page = "/info/pro-tools";
                break;
            case "stockbit academy":
                basicUrl = "https://academy";
                page = ".stockbit.com/";
                selenium.switchToWindow(2);
                selenium.waitForJavascriptToLoad();
                break;
            case "company profile":
                page = "/about";
                break;
            case "blog":
                basicUrl = "https://snips";
                page = ".stockbit.com/";
                selenium.switchToWindow(2);
                selenium.waitForJavascriptToLoad();
                break;
            case "help":
                basicUrl = "https://help";
                page = ".stockbit.com/id/";
                selenium.switchToWindow(2);
                selenium.waitForJavascriptToLoad();
                break;
            case "register":
                page = "/#/register";
                break;
            case "login":
                page = "/#/login";
                break;
        }
        Assert.assertEquals(selenium.getURL(), basicUrl + page, "Direct page is failed");
    }
}
