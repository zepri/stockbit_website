package steps;

import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobject.HeaderMenuPO;
import utilities.ThreadManager;

public class HeaderMenuSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private HeaderMenuPO menus = new HeaderMenuPO(driver);

    @When("user click on top menu {string}")
    public void user_click_on_top_menu(String menu) throws InterruptedException {
        menus.clickOnMenu(menu);
    }
}
