@stockbit @regression @smoke @header-menu

  Feature: Header Menu

   Scenario Outline: Verify header menu direct to correct address
      Given user access main page stockbit
      When user click on top menu "<Menu>"
      Then system direct to "<Page>" page

     Examples:
     | Menu       | Page                 |
     | Investing  | Start Investing      |
     | Pro Tools  | Professional Tools   |
     | Academy    | Stockbit Academy     |
     | About Us   | Company Profile      |
     | Blog       | Blog                 |
     | Help       | Help                 |
     | Sign Up    | Register             |
     | Log In     | Login                |